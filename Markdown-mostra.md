# Document de mostra de Markdown:
---
## Capçeleras
---
# Capçelera1
## Capçelera2
### Capçelera3
#### Capçelera4
##### Capçelera5
Format de text
---
- _Això està escrit en cursiva_
- **Això està en negreta**
- **_Això està en negreta i cursiva_**
- ~~Això està tatxat~~

## Taules
---
blancs  |blaus   |  grocs
--|---|--
1  |2   |4  
6  |  4 |  5

___
## Llistes
---
### Llista ordenada amb subapartats
---
1. Colors
2. Blau
3. Groc
4. Persones
   1. homes
   2. dones

### Llista no ordenada amb subllistes
---
- Colors
  - blau
  - groc
- Persones
  - homes
  - dones

### Llista checklist
- * [ ] Colors
  - * [ ] blau
  - * [ ] groc
- * [x] Persones
  - * [ ] homes
  - * [x] dones

### Cites
---
Com va dir Confucio:
>Si un problema te solució, per que et preocupes? Si un problema no te solució, per que et preocupes?

### Enllaços a recursos externes (links)
---
[Click aqui per anar a google](https://www.google.es). Això és text normal.

### Enllaços a imatges
---
Imatge de internet
![logo-markdown](imatges/logo.png)

### Introduir codi
---
Codi en la mateixa línia de text `var code = 1;`
- Aquí teniu un tros de codi en python:

~~~
x = 1
if x == 1:    
      # indented four spaces    print("x is 1.")
~~~
