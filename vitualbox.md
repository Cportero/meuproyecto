# Pràctica: Coneixent les xarxes de VirtualBox

## Baixar imatges ISO i comprovar la integritat
Es pot fer servir la comanda sha256sum nom-fitxer, i comparar amb el fitxer hash sha256 que pots encontrar a la pàgina d'Ubuntu.
>sha256sum ubuntu-20.04.1-desktop-amd64.iso
b45165ed3cd437b9ffad02a2aad22a4ddc69162470e2622982889ce5826f6e3d  ubuntu-20.04.1-desktop-amd64.iso

## Exercicis

1. Crea les màquines virtuals plantilla de Ubuntu.
>![  plantilla](imatges/plantilla.png)
2. Crea un clon enllaçat de l'Ubuntu Desktop i activa la xarxa "només Amfitrió". Posa'l en marxa.
Primer s'ha de clonar la maquina i posteriorment configurar l'adaptador de xarxa.
>![red anfitrio](imatges/red.png)

  Cal anar a les opcions generals de VirtualBox per a crear una nova xarxa Només Amfitrió. Aneu al menú fitxer, gestor de xarxes amfitrió. Pero en el meu cas ja  tenia creada la xarxa.

  >![red1](imatges/red1.png)

3. Mira quina adreça té l'equip convidat i quina adreça s'ha assignat a l'equip amfitrió a la interfície vobxnet0.
   1. Equip convidat:
    >![ip a](imatges/ip2.png)
   2. Equip anfitrió:
    >![ip1](imatges/ip1.png)
4. Fes ping des de la màquina amfitriona a la màquina convidada i des de la convidada a la amfitriona.
   1. Ping des de la màquina anfitriona a la convidada:
    >![ping2](imatges/ping2.png)
   2. Ping des de la màquina convidada a la anfitriona:
    >![ping1](imatges/ping1.png)
5. Connecta't via ssh a la màquina convidada des de l'amfitriona.
>![ssh](imatges/ssh.png)
6. Clona un ubuntu server i activa la xarxa NAT. Posa'l en marxa. Comprova que tens accés a Internet.

  Primer hem de fer la clonació enllaçada i activar la xarxa nat a les configuracions de xarxa de la màquina virtual.
  >![google](imatges/google.png)
7. Instal·la el servei ssh al server.

  Primer actulitzem el sistema:
  >sudo apt-get update

  Instal·lem el servidor OpenSSH i les seves dependències.
  >sudo apt-get install openssh-server

  Iniciem el servei.
  >sudo systemctl start sshd.service
8. Configura el reenviament de ports de la màquina virtual creada i posa 2222 al port amfitrió i 22 al port convidat.
>![1](imatges/nat.png)
9. Connecta via ssh al servidor.
>![ssh](images/2020/10/ssh.png)
