# ACL's Linux

Per crear acl's en Linux, fare servir una maquina virtual ubuntu desktop.

Hem de considerar que les operacions que podem necessitar són:

* Crear usuaris i afegir a grups: useradd, adduser
* Esborrar usuaris: useradd, adduser
* Crear grups: addgroup
* Esborrar grups: delgroup
* Afegir usuaris a grup: usermod

Per tant, primer de tot hem de crear el directori arrel i els subdirectoris i afegir fitxers.
>`mkdir -p /dir_arrel/{subdir1,subdir2}`
>`touch /dir_arrel/{fitx1,fitx2}.txt`
>`touch /dir_arrel/subdir1/{fitx3,fitx4}.txt`
>`touch /dir_arrel/subdir2/{fitx5,fitx6}`

Tenim tres grups: **sistemes**, desenvolupament i explotació, al primer grup li donarem tots els permisos.Per a això usem setfacl de la següent forma:
>`setfacl -b -k -R dir_arrel`
>`setfacl -R -m g:sistemes:rw dir_arrel/`

A continuació hem de donar permisos al grup de **desenvolupament** tant en el directori subdir1 i tot el seu contingut, com en el directori subdir2 i tot el seu contingut. No obstant això no hem de donar accés als fitxers que pengen directament de dir_arrel.
>`setfacl -R -m g:desenvolupament:rw dir_arrel/subdir1`
>`setfacl -R -m g:desenvolupament:rw dir_arrel/subdir2`

Finalment hem de donar permisos al grup d'**explotació** en el directori subdir2 i tot el seu contingut:
>`setfacl -R -m g:explotacio:rx dir_arrel/subdir2`

Per visualitzar els permisos que té cada grup sobre un directori fem servir la comanda getfacl i el nom de l'arxiu:
>![acl](imatges/acl1.png)

>![acl](imatges/acl2.png)

>![acl](imatges/acl3.png)
