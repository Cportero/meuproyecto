# Pràctica: Primers pasos amb Docker

## Exercicis
1. Prova a crear un contenidor nou i:
   1. Primer vaig crear el contenidor amb la següent comanda:
        >`docker run --name reprod --device /dev/snd:/dev/snd -it ubuntu.`
   2. Ara s'ha d'instal·lar programari necessari per baixar youtube-dl i per poder reproduit audio, com python,ffmpeg,curl,mplayer.
        >`apt-get -y insatall python ffmpeg  curl mplayer`
   3. Instal·lem youtube-dl i permissos al directori
        >`curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl`
    ![    yt](imatges/yt.png)
        `chmod a+rx /usr/local/bin/youtube-dl`
   4. Baixar audio d'un video i comprovar que es pot reproduir
       1. `youtube-dl -x --audio-format mp3 --audio-quality 0 https://www.youtube.com/watch?v=7wtfhZwyrcc -o /mp3/musica.mp3`
       2. ` mplayer /mp3/musica.mp3` per comprovar que s'escolta.
2. Guarda el contenidor com a imatge (anomenada "reproductor") per a poder utilitzar-lo amb docker run. Ara aquesta imatge contindrà tots els paquets instal·lats abans.
   1. `sudo docker commit 59e5f2149089 reproductor `
![      repr](imatges/repr.png)


3. Arrenca un contenidor amb la imatge creada com a base que executi la comanda per baixar el fitxer i després la comanda per reproduir-lo (tot a la mateixa crida del contenidor).
   1. `sudo docker run --device /dev/snd:/dev/snd --name repro -it reproductor bash -c "youtube-dl -x --audio-format mp3 --audio-quality 0 https://www.youtube.com/watch?v=fKopy74weus -o /mp3/musica.mp3 && mplayer /mp3/musica.mp3"`
4. Fes neteja de contenidors i imatges que no facis servir. Mostra TOTS els contenidors i iamtges que hi ha en local abans i després de la neteja.
   1. Ho farem amb dos comandes:
      1. docker container prune
      2. docker image prune

Abans
![abans](imatges/abans.png)
Després
![d](imatges/despres.png)
    
